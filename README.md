# Frontend Mentor - QR code component

![Design preview for the QR code component coding challenge](./design/desktop-preview.jpg)

My first ever website - a simple page with a light/dark mode button, which has a QR-Code in it.

https://qr-code-redo.vercel.app/
